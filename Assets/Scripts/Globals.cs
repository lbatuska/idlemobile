﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Globals : MonoBehaviour
{
    // ---------- TYPES OF TILES ----------
    public enum BaseType
    {
        Grassland, Forest, SaltyWater, SandDunes, Water
    };
    // ---------- TYPES OF TILES ----------
    // ---------- TYPES OF RESOURCES ----------
    public enum ResourceTypes
    {
        Food, Resorce, Science, Prestige
    };
    // ---------- TYPES OF RESOURCES ----------
    // ---------- BUILDING AND BUYING TILES ----------
    public GameObject[] buildingTypes; // All types of buildable Buildings
    public float[] nextBuildingPrice; // Keep Track of all the different Types of building prices
    public float[] lastBuildingPrice; // If Building is sold we need the last bought price
    public float nextTilePrice; // Price for the next Tile to buy
    public float lastTilePrice; // If Tile is sold we need the last bought price
    // ---------- BUILDING AND BUYING TILES ----------
    // ---------- MAP ----------
    public GameObject[] tileTypes; // All types of Tiles
    //public GameObject fog; // "Fog" to cover the map
    //public GameObject unlockedFog; // "Fog" to cover the map
    public int mapSizeX = 20, mapSizeY = 20; // Map Size
    public GameObject[,] generatedMap; // Actually Generated Map
    //public GameObject[,] generatedMapFog; // Actually Generated Map
    //public GameObject[,] generatedMapUnlockedFog; // Actually Generated Map
    public Map mapGeneratorObj;
    // ---------- MAP ----------
    // ---------- GLOBAL RESOURCE VALUES ----------
    public float Food = 1000;
    public float FoodThousands = 0;
    public float Resource = 1000;
    public float ResourceThousands = 0;
    public float Science = 1000;
    public float ScienceThousands = 0;
    public float Prestige = 1000;
    public float PrestigeThousands = 0;
    // ---------- GLOBAL RESOURCE VALUES ----------
    // ---------- GLOBAL RESOURCE PER SEC VALUES ----------
    public float FoodPerSec = 1;
    public float ResourcePerSec = 1;
    public float SciencePerSec = 1;
    public float PrestigePerSec = 1;
    // ---------- GLOBAL RESOURCE PER SEC VALUES ----------
    // ---------- GLOBAL RESOURCE UI ----------
    public GameObject FoodDispalyObj;
    public GameObject ResourceDispalyObj;
    public GameObject ScienceDispalyObj;
    public GameObject PrestiDispalygeObj;

    TextMeshProUGUI FoodDispalytxt;
    TextMeshProUGUI ResourceDispalytxt;
    TextMeshProUGUI ScienceDispalytxt;
    TextMeshProUGUI PrestigeDispalytxt;
    // ---------- GLOBAL RESOURCE UI ----------

    public void Awake()
    {
        // ---------- GET RESOURCE UI TEXTS ----------
        FoodDispalytxt = FoodDispalyObj.GetComponentInChildren<TextMeshProUGUI>();
        ResourceDispalytxt = ResourceDispalyObj.GetComponentInChildren<TextMeshProUGUI>();
        ScienceDispalytxt = ScienceDispalyObj.GetComponentInChildren<TextMeshProUGUI>();
        PrestigeDispalytxt = PrestiDispalygeObj.GetComponentInChildren<TextMeshProUGUI>();
        // ---------- GET RESOURCE UI TEXTS ----------
        // ---------- INITIALIZE MAP ----------
        generatedMap = new GameObject[mapSizeX, mapSizeY];
        //generatedMapFog = new GameObject[mapSizeX, mapSizeY];
        //generatedMapUnlockedFog = new GameObject[mapSizeX, mapSizeY];
        ResetMap();
        // ---------- INITIALIZE MAP ----------
        // ---------- INITIALIZE PRICES ----------
        nextBuildingPrice = new float[buildingTypes.Length];
        lastBuildingPrice = new float[buildingTypes.Length];
        nextTilePrice = 100;
        lastTilePrice = 0;
        // ---------- INITIALIZE PRICES ----------
    }
    public void TileBought(GameObject tile)
    {
        tile.transform.position += new Vector3(0.0f, 0.3f, 0.0f);

        int originalX = (int)tile.transform.position.x;
        int originalY = (int)tile.transform.position.z;

        var InBoundCoords = CalculateIfInBoundsNoDiagonal(originalX, originalY);

        foreach (var item in InBoundCoords)
        {
            generatedMap[(int)item.x, (int)item.y].GetComponent<BaseTile>().Unlocked = true;
            //Destroy(generatedMapFog[(int)item.x, (int)item.y]);
            //Destroy(generatedMapUnlockedFog[(int)item.x, (int)item.y]);
            if(generatedMap[(int)item.x, (int)item.y].transform.position.y<-0.3f)
            {
                generatedMap[(int)item.x, (int)item.y].transform.position += new Vector3(0.0f, 0.5f, 0.0f);
            }

            // var biggerCoords = CalculateIfInBoundsNoDiagonal((int)item.x, (int)item.y);
            // foreach (var item2 in biggerCoords)
            // {
            //     //Destroy(generatedMapFog[(int)item2.x, (int)item2.y]);
            // }
        }
    }
    public IEnumerable<Vector2> CalculateIfInBoundsNoDiagonal(int originalX, int originalZ) // Returns a List of Vector2 of positions (X +1 & -1)&(Y +1 & -1) [diagonals are excluded]
    {
        int originalY = originalZ;
        List<Vector2> InBoundCoords = new List<Vector2>();

        if (originalX - 1 >= 0)
        {
            InBoundCoords.Add(new Vector2(originalX - 1, originalY));
        }
        if (originalX + 1 <= mapSizeX)
        {
            InBoundCoords.Add(new Vector2(originalX + 1, originalY));
        }
        if (originalY - 1 >= 0)
        {
            InBoundCoords.Add(new Vector2(originalX, originalY - 1));
        }
        if (originalY + 1 <= mapSizeX)
        {
            InBoundCoords.Add(new Vector2(originalX, originalY + 1));
        }
        return InBoundCoords;
    }
    public void BuildingBuilt(GameObject building)
    {
        // Resource collectors should be optimized
        // Resource Per Sec Should be recalculated
    }
    public void ResetMap()
    {
        mapGeneratorObj.Reset(mapSizeX, mapSizeY, tileTypes, generatedMap, TileBought);//, generatedMapFog, fog, generatedMapUnlockedFog, unlockedFog);
    }
   
    void UpdateResourceUI()
    {
        // First calculate values

        Food += FoodPerSec * Time.deltaTime;
        if (Food / 1000 >= 1)
        {
            Food = Food % 1000;
            FoodThousands += 1;
        }
        Resource += ResourcePerSec * Time.deltaTime;
        if (Resource / 1000 >= 1)
        {
            Resource = Resource % 1000;
            ResourceThousands += 1;
        }
        Science += SciencePerSec * Time.deltaTime;
        if (Science / 1000 >= 1)
        {
            Science = Science % 1000;
            ScienceThousands += 1;
        }

        // Display values
        FoodDispalytxt.text = Food.ToString("0.0");
        ResourceDispalytxt.text = Resource.ToString("0.0");
        ScienceDispalytxt.text = Science.ToString("0.0");
        PrestigeDispalytxt.text = Prestige.ToString("0.0");
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        UpdateResourceUI();
    }
}
