﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Globals;

public class BaseBuilding : MonoBehaviour
{
    public BaseType type;
    public ResourceTypes restype;

    public float upgradeprice = 0;
    public float level = 1;
    public float sellprice;
    public float nextprice = 0;
    public float resourcepersec;
    public bool generator;
    
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {

    }
}
