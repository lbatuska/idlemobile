﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BuildMenu : MonoBehaviour
{
    public Globals globals;

    public GameObject builderButtonPref;
    List<GameObject> buttons_instantiated = new List<GameObject>();
    public bool buttonWindowActive;

    public void MakeButtons(GameObject Selectedtile)
    {
        this.GetComponent<RectTransform>().sizeDelta = new Vector2(0, 0);
        this.transform.position = new Vector3(Selectedtile.transform.position.x + 1.0f, 2.0f, Selectedtile.transform.position.z);
        Destroy();
        //-------
        PopulateButtonData(Selectedtile);
        return;
    }

    void PopulateButtonData(GameObject tile)
    {
        float accumulatedHeight = builderButtonPref.GetComponent<RectTransform>().rect.height;
        int accumulator = 0;

        // Is Unlocked?
        if (!tile.GetComponent<BaseTile>().Unlocked) // Not Unlocked
        {
            // Unlockable? -> Globals Should Deal with unlocking
        }
        else
        {
            // Do we already own it?
            if (!tile.GetComponent<BaseTile>().Bought) // Didn't buy it yet
            {
                // It is Unlocked but not bought -> Buy land
                accumulator++;
                GameObject btn = Instantiate(builderButtonPref, this.transform);
                btn.GetComponentInChildren<TextMeshProUGUI>().text = "Buy Land";
                buttons_instantiated.Add(btn);

                if (globals.GetComponent<Globals>().Resource >= globals.GetComponent<Globals>().nextTilePrice)
                {
                    btn.GetComponent<Button>().onClick.AddListener(() => // Create OnClick function
                    {
                        
                        globals.GetComponent<Globals>().TileBought(tile); // Unlock Tiles Around
                        tile.GetComponent<BaseTile>().Bought = true; // Bought
                        globals.GetComponent<Globals>().Resource -= globals.GetComponent<Globals>().nextTilePrice; // Subtract Price of Tile
                        Destroy(); // Destroy the buttons
                        MakeButtons(tile);
                    });
                }
                // Not enough resources, show button but disabled
                else
                {
                    btn.GetComponent<Button>().interactable = false;
                }

            }
            else
            {
                // Has Building?
                if (tile.GetComponent<BaseTile>().hasBuilding)
                {
                    // Upgrade 
                    accumulator++;
                    GameObject btn = Instantiate(builderButtonPref, this.transform);
                    btn.GetComponentInChildren<TextMeshProUGUI>().text = "Upgrade " + tile.GetComponentInChildren<BaseBuilding>().upgradeprice.ToString();
                    buttons_instantiated.Add(btn);
                    // Remove
                }
                // No Building
                else
                {
                    foreach (var item in globals.buildingTypes)
                    {
                        // If same building type as base tile
                        var baseBuldingComponent = item.GetComponent<BaseBuilding>();
                        if (baseBuldingComponent.type == tile.GetComponent<BaseTile>().basetype) // Check if Building.Type == SelectedTile.Type
                        {
                            accumulator++; // Number of buttons
                            GameObject btn = Instantiate(builderButtonPref, this.transform);
                            btn.GetComponentInChildren<TextMeshProUGUI>().text = baseBuldingComponent.type.ToString();
                            buttons_instantiated.Add(btn);
                            // If we have enough resources
                            if (globals.GetComponent<Globals>().Resource >= baseBuldingComponent.nextprice) // Shoul make a decision based on Resource we have to spend
                            {
                                btn.GetComponent<Button>().onClick.AddListener(() => // Create OnClick function
                                {
                                    Instantiate(item, tile.transform);
                                    tile.GetComponent<BaseTile>().hasBuilding = true;
                                    globals.GetComponent<Globals>().Resource -= baseBuldingComponent.nextprice;
                                    Destroy();
                                    MakeButtons(tile);
                                });
                            }
                            // Not enough resources, show button but disabled
                            else
                            {
                                btn.GetComponent<Button>().interactable = false;
                            }
                        }
                    }
                }
            }
        }

        this.GetComponent<RectTransform>().sizeDelta = new Vector2(accumulator == 0 ? 0 : builderButtonPref.GetComponent<RectTransform>().rect.width, accumulator == 0 ? 0 : accumulatedHeight * accumulator);
    }

    public void Destroy()
    {
        if (buttons_instantiated.Count > 0)
        {
            foreach (var item in buttons_instantiated)
            {
                Destroy(item);
            }
        }
        buttons_instantiated = new List<GameObject>();
        buttonWindowActive = false;
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
