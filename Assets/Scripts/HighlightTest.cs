﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class HighlightTest : MonoBehaviour
{

    #region Variables
    // Probably Highliter stuff can go to the GLOBALS
    public Material highlightMaterial; //material used to highlight gameobject.
    public GameObject highlightObject; //the gameobject to highlight
    public GameObject highlighterObject; //the gameobject to highlight
    // Probably Highliter stuff can go to the GLOBALS

    public Camera mainCamera;


    public GameObject builderMenu;
    BuildMenu builderMenu_BuildMenu_Component;

    #endregion

    #region Variables for movement

    public float panSpeed = 5f;
    public Vector2 panLimit = new Vector2(100, 100);
    public float scrollSpeed = 2f;
    public float minY = 6f;
    public float maxY = 15f;
    #endregion


    Vector3 lastMousePosition;
    DateTime touchtime;
    DateTime releasetime;


    Touch touch;
    bool touchmoved;

    bool touchingUI = false;

    private void LateUpdate()
    {
        if (EventSystem.current.IsPointerOverGameObject())
        {
            touchingUI = true;
            return;
        }

        for (int touchIndex = 0; touchIndex < Input.touchCount; touchIndex++)
        {
            Touch touch = Input.GetTouch(touchIndex);
            if (EventSystem.current.IsPointerOverGameObject(touch.fingerId))
            {
                touchingUI = true;
                return;
            }
        }

        touchingUI = false;
    }

    void Update()
    {
        if (touchingUI)
        {
            return;
        }

        // Zoom
        Vector3 pos = transform.position;

        if (Input.touchCount == 2)
        {
            Debug.Log("2 Touches");
            Touch touchZero = Input.GetTouch(0);
            Touch touchOne = Input.GetTouch(1);

            Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
            Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

            float prevMagnitude = (touchZeroPrevPos - touchOnePrevPos).magnitude;
            float currentMagnitude = (touchZero.position - touchOne.position).magnitude;

            float difference = currentMagnitude - prevMagnitude;

            pos.y = Mathf.Clamp(mainCamera.transform.position.y - difference * 0.01f, minY, maxY);
        }
        else
    // Zoom

    // Touching or dragging
    if (Input.touchCount == 1)
        {
            touch = Input.GetTouch(0);
            switch (touch.phase)
            {
                case TouchPhase.Began:
                    Debug.Log("Touch Began");
                    touchmoved = false;
                    break;
                case TouchPhase.Moved:
                    Debug.Log("Touch Moved");
                    touchmoved = true;
                    float curPanSpeed = panSpeed * 7 / mainCamera.transform.position.y;
                    Debug.Log(curPanSpeed);
                    pos.z -= touch.deltaPosition.y / curPanSpeed * Time.deltaTime;
                    pos.x -= touch.deltaPosition.x / curPanSpeed * Time.deltaTime;
                    // Limit movement at the edges
                    pos.x = Mathf.Clamp(pos.x, -panLimit.x, panLimit.x);
                    pos.z = Mathf.Clamp(pos.z, -panLimit.y, panLimit.y);
                    break;
                case TouchPhase.Stationary:
                    break;
                case TouchPhase.Ended:
                    if (!touchmoved)
                    {
                        Debug.Log("Touch did not move");
                        // We need to actually hit an object
                        RaycastHit hit_01;
                        if (Physics.Raycast(mainCamera.ScreenPointToRay(touch.position), out hit_01, 100.0f))
                        {
                            //if (hit_01.transform && hit_01.transform.gameObject.activeSelf)
                            if (hit_01.transform.tag == "Selectable" && hit_01.transform.gameObject.activeSelf)
                            //if (hit_01.transform.GetComponent<BaseTile>() && hit_01.transform.gameObject.activeSelf)
                            {
                                if (highlightObject != hit_01.transform.parent.gameObject)
                                {
                                    highlighterObject.SetActive(false);
                                    highlightObject = hit_01.transform.parent.gameObject;
                                    highlighterObject.transform.position = new Vector3(highlightObject.transform.position.x, 0.8f, highlightObject.transform.position.z);
                                    highlighterObject.SetActive(true);
                                    if (builderMenu_BuildMenu_Component.buttonWindowActive)
                                        builderMenu_BuildMenu_Component.Destroy();
                                    builderMenu_BuildMenu_Component.MakeButtons(highlightObject);
                                    builderMenu.SetActive(true);
                                }
                                else
                                {
                                    highlightObject = null;
                                    highlighterObject.SetActive(false);
                                    builderMenu_BuildMenu_Component.Destroy();
                                    builderMenu.SetActive(false);
                                }
                            }
                            else
                            {
                                builderMenu_BuildMenu_Component.Destroy();
                                builderMenu.SetActive(false);
                            }
                        }
                    }
                    Debug.Log("Touch Ended");
                    break;
                case TouchPhase.Canceled:
                    break;
                default:
                    break;
            }
            //if (touch.phase == TouchPhase.Began)
            //{
                
            //}
            //if (touch.phase == TouchPhase.Moved)
            //{
                
            //}
            //if (touch.phase == TouchPhase.Ended)
            //{
                
            //}
        }
        // Touching or dragging

        transform.position = pos;

    }

    // Start is called before the first frame update
    void Start()
    {
        builderMenu_BuildMenu_Component = builderMenu.GetComponent<BuildMenu>();
    }
}
