﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Map : MonoBehaviour
{
    bool HasStartingPoint = false;
    public void Reset(int sizeX, int sizeY, GameObject[] tiletypes, GameObject[,] map, Action<GameObject> f)//, GameObject[,] fogmap, GameObject fog, GameObject[,] unlockedfogmap, GameObject unlockedfog)
    {
        //Vector2 startingpoint = new Vector2();
        System.Random rnd = new System.Random();
        for (int i = 0; i < sizeX; i++)
        {
            for (int j = 0; j < sizeY; j++)
            {
                // If there is a Tile Destroy it
                Destroy(map[i, j]);
                // Generate a completely random Tile (Later should be changed to a bit less random)
                map[i, j] = Instantiate(tiletypes[rnd.Next(0, tiletypes.Length)], new Vector3(i, -0.8f, j), Quaternion.identity, this.transform); // Make the tiles the Children of the generator
                //fogmap[i, j] = Instantiate(fog, map[i, j].transform);
                //unlockedfogmap[i, j] = Instantiate(unlockedfog, map[i, j].transform);

                //map[i, j].GetComponentInChildren<MeshRenderer>().material.SetFloat("_Metallic", 0.7f);

                // if (!HasStartingPoint)
                // {
                //     if (map[i, j].GetComponent<BaseTile>().basetype == Globals.BaseType.Grassland && rnd.Next(1, 11) == 1)
                //     {
                //         map[i, j].GetComponent<BaseTile>().Unlocked = true;
                //         map[i, j].transform.position += new Vector3(0.0f, 0.5f, 0.0f);
                //         f(map[i,j]);
                //         //startingpoint = new Vector2(i, j);
                //         HasStartingPoint = true;
                //         //Destroy(fogmap[i, j]);
                //         //Destroy(unlockedfogmap[i, j]);
                //     }
                //     else
                //     {
                //     }
                // }
            }
        }
        while (!HasStartingPoint) // If there is no starting point
        {
            for (int i = 0; i < sizeX; i++)
            {
                if (!HasStartingPoint)
                {
                    for (int j = 0; j < sizeY; j++)
                    {
                        if (!HasStartingPoint)
                        {
                            if (map[i, j].GetComponent<BaseTile>().basetype == Globals.BaseType.Grassland && rnd.Next(1, 11) == 1)
                            {
                                map[i, j].GetComponent<BaseTile>().Unlocked = true;
                                map[i, j].transform.position += new Vector3(0.0f, 0.5f, 0.0f);
                                map[i, j].GetComponent<BaseTile>().Bought = true;
                                f(map[i,j]);
                                //startingpoint = new Vector2(i, j);
                                HasStartingPoint = true;
                                //Destroy(fogmap[i, j]);
                                //Destroy(unlockedfogmap[i, j]);
                            }
                        }
                        else
                        {
                            break;
                        }
                    }
                }
                else
                {
                    break;
                }
            }
        }
        //map[(int)startingpoint.x, (int)startingpoint.y].GetComponentInChildren<MeshRenderer>().material.SetFloat("_Metallic", 0.0f);
    }
}